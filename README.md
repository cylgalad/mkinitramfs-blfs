# mkinitramfs-blfs #

Here are all the files necessary to create a working initramfs for a [Beyond Linux from Scratch](http://www.linuxfromscratch.org/blfs/) installation with cryptsetup support (full disk encryption).
You need to install cryptsetup and lvm2 and any raid stuff like md/dm.

This was adapted from already existing mkinitramfs from BLFS v7.6-systemd, also using code from an older mkinitramfs by Bryan Kadzban (included as usr/sbin/mkinitramfs.old).

Note that this is quite a simple "hack" to get a LFS/BLFS system to boot when installed on an LVM root/swap over a cryptsetup/LUKS partition.

** License: ** GPL-3